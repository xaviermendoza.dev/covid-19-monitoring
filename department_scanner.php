


<?php 
session_start();
 
?>
<?php


if($_SESSION['department'] === NULL){

header('Location: index.php');


}else{

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="css/index.css">

    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">
   

    <title>Administration and Covid 19 Monitor</title>
</head>
<body onload="startTime()">




  <div class="maincontainer" style="background-color:#7F1517;"> 
  <div class="nav-section">
       <div id="van"  class="row">
            <div class="col">
                <h2>The Philippine Women's Academy</h2>
            </div>
            <div class="col">
                <ul>

                    <li id="time">Time Here</li>
                    <li ><?php echo date("m-d-Y l"); ?></li>

                </ul>


            </div>
        </div>
       
        <div id="van2"  class="row">
        
            <div class="col">
                <ul>
                    
                    <li id="time">Time Here</li>
                    <li ><?php echo date("m-d-Y l"); ?></li>
                    <li class="lef"><a><img src="img/pwu-logo.png" alt=""></a></li> 

                </ul>


            </div>
            
        </div>


        
    </div>
    <br><br><br><br>
    <div class="scanner">
      <center>
      <div class="con-scan">






<?php

include('class_lib.php');
   if($_SERVER['REQUEST_METHOD']=="POST"){

       $qr=$_POST['qrcode'];
      

       $ar=explode(',', $qr);
       $fname = $ar[1];
       $mname = $ar[2];
       $lname = $ar[3];


     
    

         
       

       
       date_default_timezone_set('Asia/Manila');
       $dov= date("h:i:sa");
       $de =date("Y/m/d");
       
      


       
   


       $conn = getConnection();
       $sql = "SELECT * FROM `tbl_logs` WHERE `qrcode`='$qr' AND `timeout` =''";
       $result = $conn->query($sql);
       $rowcount=count($result->fetchAll());
   
       
       if($rowcount == 0){

         $data = custom_query("SELECT * FROM `tbl_employee` WHERE qrcode= '$qr' ");
         foreach ($data as $row) {
           $departvisit= $_SESSION['department'];
           $department= $row['department'];
           $first= $fname;
           $mid= $mname;
           $last= $lname;
           $email= $row['email'];
           $imgpath= $row['img_path'];
           $address= $row['address'];
           $contact =$row['contact'];
           $datevisit =$de;
           $birthday = $row['birthday'];
           $qrcode= $qr;
           $timein=$dov;
           
           
           
         }
           
           if($email == ""){
             //no record found
             
           }else{
             $array = array(
               
               'department_visit'=>$departvisit,
               'qrcode'=>$qrcode,
               'fname'=>$first,
               'mname'=>$mid,
               'lname'=>$last,
               'address'=>$address,
               'contact'=>$contact,
               'email'=>$email,
               'birthday'=>$birthday,
               'department'=>$department,
               'timein'=>$timein,
               'date'=>$datevisit,
             );

               if(insert($array,'tbl_logs')){
                 echo "<img src='" .$imgpath ."' height='250'> ";
                 echo "<p></p>";
                 echo "<div class='details'>";
                 echo "<p>Name : ".$lname.", ".$fname." ".$mname."</p>";

                 echo "<p>Address : ".$address."</p>";

                 echo "<p>Time in : ".$dov."</p>";
                 echo "</div>";
                   
               }else{
                   ?>
                   <script>alert('Error');</script>
                   <?php 
               }
              
           }
      

        

       }else{
        $data = custom_query("SELECT * FROM `tbl_employee` WHERE qrcode= '$qr' ");
        foreach ($data as $row) {
          $img_path = $row['img_path'];
          $address= $row['address'];
        }


         $array = array(
           'timeout'=>$dov

         );
         if(updatelog($array,$qr,'tbl_logs',$_SESSION['department'])){
          echo "<img src='" .$img_path ."'  height='250'>";
          echo "<p></p>";
          echo "<div class='details'>";
          echo "<p>Name : ".$lname.", ".$fname." ".$mname."</p>";

          echo "<p>Address : ".$address."</p>";

          echo "<p>Time Out : ".$dov."</p>";
          echo "</div>";

          

               

           
       }else{
         echo "error";
       }





   }
 }
 
  ?>















<p></p>

        <form id="form" method="POST" action="">
        <div class="form-group row">
          <div class="col-sm-12">
            <input class="form-control" id="here"  placeholder="Scan QRcode Here..." name="qrcode" type="text" tabindex="1" required autofocus>
          </div>
        </div>
          <p id="demo"></p>
          </form>

          
        </form>


      </div>
      </center>
      <div class="back">
      <a href="logout.php"><img src="img/left-arrow.png" height="16" alt=""> Exit Application</a>
    </div>
    </div>
   

  </div>












  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script>




  $(document).ready(function() {
var timer = '';
  $('input#here').keypress(function() {
      var _this = $(this); // copy of this object for further usage
      clearTimeout(timer);
      timer = setTimeout(function() {
              //alert(_this.val());
              $("#form").submit();
      }, 1000);
  });
  });





</script>


<script>
function startTime() {
  var today = new Date();
  var h = today.getHours();
  var m = today.getMinutes();
  var s = today.getSeconds();
  m = checkTime(m);
  s = checkTime(s);
  document.getElementById('time').innerHTML =
  h + ":" + m + ":" + s;
  var t = setTimeout(startTime, 500);
}
function checkTime(i) {
  if (i < 10) {i = "0" + i};  // add zero in front of numbers < 10
  return i;
}
</script>

<?php  } ?>

</body>
</html>