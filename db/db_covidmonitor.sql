-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Feb 02, 2021 at 01:03 PM
-- Server version: 10.4.8-MariaDB
-- PHP Version: 7.3.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_covidmonitor`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_admin`
--

CREATE TABLE `tbl_admin` (
  `ID` int(11) NOT NULL,
  `username` text NOT NULL,
  `password` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_admin`
--

INSERT INTO `tbl_admin` (`ID`, `username`, `password`) VALUES
(1, 'admin', '21232f297a57a5a743894a0e4a801fc3');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_department`
--

CREATE TABLE `tbl_department` (
  `ID` int(11) NOT NULL,
  `d_name` text NOT NULL,
  `d_location` text NOT NULL,
  `d_password` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_department`
--

INSERT INTO `tbl_department` (`ID`, `d_name`, `d_location`, `d_password`) VALUES
(2, 'testdep', 'test,tuguegarao', '098f6bcd4621d373cade4e832627b4f6'),
(5, 'test2', 'test', '098f6bcd4621d373cade4e832627b4f6');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_employee`
--

CREATE TABLE `tbl_employee` (
  `ID` int(11) NOT NULL,
  `employee_id` text NOT NULL,
  `department` text NOT NULL,
  `qrcode` text NOT NULL,
  `fname` text NOT NULL,
  `lname` text NOT NULL,
  `mname` text NOT NULL,
  `address` text NOT NULL,
  `email` text NOT NULL,
  `contact` text NOT NULL,
  `birthday` text NOT NULL,
  `img_path` text NOT NULL,
  `fever` text NOT NULL,
  `headache` text NOT NULL,
  `sthroat` text NOT NULL,
  `tassmell` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_employee`
--

INSERT INTO `tbl_employee` (`ID`, `employee_id`, `department`, `qrcode`, `fname`, `lname`, `mname`, `address`, `email`, `contact`, `birthday`, `img_path`, `fever`, `headache`, `sthroat`, `tassmell`) VALUES
(1, 'test123', 'testdep', '1,test,test,test', 'test', 'test', 'test', 'testing add', 'test@test.com', '1234', '10/01/1999', '../img/qrcode/test123.png', 'No', 'No', 'No', 'No'),
(10, '', 'Visitor', '2,Xavier,Asuncion,Mendoza', 'Xavier', 'Mendoza', 'Asuncion', '14reyes', 'asdasd@gmail.com', '1231231234124', '2000-10-02', 'img/qrcode/60150f21458c7.png', 'yes', '', '', ''),
(11, '', 'Visitor', '11,Xavier,Asuncion,Mendoza', 'Xavier', 'Mendoza', 'Asuncion', '14reyes', 'asdasd@gmail.com', '1231231234124', '2000-10-02', 'img/qrcode/60150f2d65861.png', 'yes', '', '', ''),
(12, '', 'Visitor', '12,test,test,test', 'test', 'test', 'test', 'asdasdas', 'etstert@sdf.com', '123123', '0012-12-12', 'img/qrcode/601559a7d99b8.png', 'yes', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_logs`
--

CREATE TABLE `tbl_logs` (
  `ID` int(11) NOT NULL,
  `department_visit` text NOT NULL,
  `qrcode` text NOT NULL,
  `fname` text NOT NULL,
  `lname` text NOT NULL,
  `mname` text NOT NULL,
  `address` text NOT NULL,
  `contact` text NOT NULL,
  `email` text NOT NULL,
  `birthday` text NOT NULL,
  `department` text NOT NULL,
  `timein` text NOT NULL,
  `timeout` text NOT NULL,
  `date` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_logs`
--

INSERT INTO `tbl_logs` (`ID`, `department_visit`, `qrcode`, `fname`, `lname`, `mname`, `address`, `contact`, `email`, `birthday`, `department`, `timein`, `timeout`, `date`) VALUES
(9, 'testdep', '1,test,test,test', 'test', 'test', 'test', 'testing add', '1234', 'test@test.com', '10/01/1999', 'testdep', '11:38:53am', '', '2021/01/31'),
(11, 'testdep', '11,Xavier,Asuncion,Mendoza', 'Xavier', 'Mendoza', 'Asuncion', '14reyes', '1231231234124', 'asdasd@gmail.com', '2000-10-02', 'Visitor', '11:43:53am', '11:43:57am', '2021/01/31');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_admin`
--
ALTER TABLE `tbl_admin`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `tbl_department`
--
ALTER TABLE `tbl_department`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `tbl_employee`
--
ALTER TABLE `tbl_employee`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `tbl_logs`
--
ALTER TABLE `tbl_logs`
  ADD PRIMARY KEY (`ID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_admin`
--
ALTER TABLE `tbl_admin`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tbl_department`
--
ALTER TABLE `tbl_department`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `tbl_employee`
--
ALTER TABLE `tbl_employee`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `tbl_logs`
--
ALTER TABLE `tbl_logs`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
