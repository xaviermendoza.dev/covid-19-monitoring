<?php
include('header.php');

?>



<?php 


?>
    <!-- Page Wrapper -->
    <div id="wrapper">

    <?php
    
    include('nav.php');
    
    ?>

        <!-- Content Wrapper -->
        <div id="content-wrapper" class="d-flex flex-column">

            <!-- Main Content -->
            <div id="content">

            <?php
             
             include('topbar.php');
             
             ?>

                <!-- Begin Page Content -->
                <div class="container-fluid">

                    <!-- Page Heading -->
                    <div class="d-sm-flex align-items-center justify-content-between mb-4">
                        <h1 class="h3 mb-0 text-gray-800">Logs</h1>

                     
                    </div>

                    <!-- Content Row -->



                   
                   
                            <!-- Department List -->
                        <div class="row">
                               

                        <div class="col-lg-12 mb-4">
                        <div class="card shadow mb-4">
                        <div class="card-header py-3">'
                        
                            <h6 class="m-0 font-weight-bold text-primary">Log Details</h6>
                            
                        </div>
                   
                        <div class="card-body">
                           
                            <form action="" method="POST">
                                <?php
                                
                                    $id=$_GET['id'];
                                    $data = custom_query("SELECT * FROM `tbl_logs` WHERE id=$id");
                                    foreach ($data as $row) {
                                       
                                   
                                ?>
                                <div class="form-group row">
                                    <div class="col-sm-6">
                                        <label for="">Department Visit</label>
                                        <input type="text" class="form-control form-control-user" value="<?php echo $row['department_visit']; ?>" name="departmentvisit" 
                                            placeholder="Department Visit" readonly>
                                    </div>
                                    <div class="col-sm-6">
                                    <label for="">Department</label>
                                        <input type="text" class="form-control form-control-user" value="<?php echo $row['department']; ?>" name="department" 
                                            placeholder="Department From" readonly/>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-sm-4">
                                    <label for="">Time In</label>
                                        <input type="text" class="form-control form-control-user" value="<?php echo $row['timein']; ?>" name="timein" 
                                            placeholder="Timein" readonly>
                                    </div>
                                    <div class="col-sm-4">
                                    <label for="">Time Out</label>
                                        <input type="text" class="form-control form-control-user" value="<?php echo $row['timeout']; ?>" name="timeout" 
                                            placeholder="Timeout" readonly/>
                                    </div>
                                    <div class="col-sm-4">
                                    <label for="">Date</label>
                                        <input type="text" class="form-control form-control-user" value="<?php echo $row['date']; ?>" name="date" 
                                            placeholder="Date" readonly/>
                                    </div>
                                </div>
                              
                                <div class="form-group row">
                                    <div class="col-sm-4">
                                    <label for="">First Name</label>
                                        <input type="text" class="form-control form-control-user" value="<?php echo $row['fname']; ?>" name="fname" 
                                            placeholder="First Name" readonly>
                                    </div>
                                    <div class="col-sm-4">
                                    <label for="">Middle Name</label>
                                        <input type="text" class="form-control form-control-user" value="<?php echo $row['mname']; ?>" name="mname" 
                                            placeholder="Middle Name" readonly/>
                                    </div>
                                    <div class="col-sm-4">
                                    <label for="">Last Name</label>
                                        <input type="text" class="form-control form-control-user" value="<?php echo $row['lname']; ?>" name="lname" 
                                            placeholder="Last Name" readonly/>
                                    </div>
                                </div>


                                <div class="form-group row">
                                    <div class="col-sm-12 mb-3 mb-sm-0">
                                    <label for="">Email</label>
                                        <input type="email" class="form-control form-control-user" value="<?php echo $row['email']; ?>" name="email" 
                                            placeholder="Email" readonly/>
                                    </div>
                                   
                                </div>


                                <div class="form-group row">
                                    <div class="col-sm-12 mb-3 mb-sm-0">
                                    <label for="">Address</label>
                                        <input type="text" class="form-control form-control-user" value="<?php echo $row['address']; ?>" name="address" 
                                            placeholder="Address" readonly/>
                                    </div>
                                   
                                </div>


                                <div class="form-group row">
                                    <div class="col-sm-6 mb-3 mb-sm-0">
                                    <label for="">Contact Number</label>
                                        <input type="number" class="form-control form-control-user" value="<?php echo $row['contact']; ?>" name="contact" 
                                            placeholder="Contact Number" readonly/>
                                    </div>
                                    
                                    <div class="col-sm-6 mb-3 mb-sm-0">
                                    <label for="">Birthday</label>
                                        <input type="text" value="<?php echo $row['birthday']; ?>" disabled class="form-control form-control-user" name="bday" 
                                            placeholder="Birthday" readonly/>
                                    </div>
                                   
                                </div>


                                <br>


                                <a class="btn btn-secondary" href="log_list.php" type="button">Back</a>



                                <?php  } ?>
                            </form>
                        </div>
                    </div>
                        </div>
                    </div>

                </div>
                <!-- /.container-fluid -->

            </div>
            <!-- End of Main Content -->

           
<?php 
    include('footer.php');
?>