<?php
include('header.php');

?>

<?php 
if($_SERVER['REQUEST_METHOD']== "POST"){


    $sql = custom_query("SELECT * FROM `tbl_employee` ORDER BY  `ID` DESC LIMIT 1");
    foreach ($sql as $row3) {
       
    }
    
    $id = $row3['ID'] +1 ;


        
      

        

        $email = $_POST['email'];
        $contact = $_POST['contact'];
        $address=$_POST['address'];
        $empid = $_POST['empid'];
        $dept = $_POST['department'];
        $fname = $_POST['fname'];
        $mname = $_POST['mname'];
        $lname = $_POST['lname'];
        $birthday = $_POST['bday'];
        $fever = $_POST['fever'];
        $headache = $_POST['headache'];
        $sthroat = $_POST['sthroat'];
        $tassmell = $_POST['tassmell'];


        
        $qrcode = $id .",". $_POST['fname'].",".$_POST['mname'].",".$_POST['lname'];



        $str= "select * from tbl_employee where employee_id=:u";
        $cm=$conn->prepare($str);
        $cm->bindParam(':u', $empid);
        $cm->execute();
        $usersas = $cm->rowcount();

        if ($usersas == 0) {
            require_once('../phpqrcode/qrlib.php');

            $path='../img/qrcode/';
            $file= $path.uniqid().".png";


            $text= $qrcode;

            QRcode::png($text, $file, 'L', 10);


            
            $array = array(
                
                'employee_id'=>$empid,
                'department'=>$dept,
                'fname'=>$fname,
                'mname'=>$mname,
                'lname'=>$lname,
                'birthday'=>$birthday,
                'email'=>$email,
                'contact' => $contact,
                'address' => $address,
                'img_path'=> $file,
                'fever'=> $fever,
                'headache'=> $headache,
                'sthroat'=> $sthroat,
                'tassmell'=> $tassmell,
                'qrcode'=> $qrcode
            );

            if(insert($array,'tbl_employee')){
                ?>
                <script>alert('Employee Sucessfully Added');
                window.location.href = './employee_list.php';
                </script>
                <?php 
                
            }else{
                ?>
                <script>alert('Employee not Added');</script>
                <?php 
            }
           
        }else{
            ?>
            <script>alert('Employee ID Already Registered');</script>
            <?php 
        }
        

        



       



}

?>


<?php 


?>
    <!-- Page Wrapper -->
    <div id="wrapper">

    <?php
    
    include('nav.php');
    
    ?>

        <!-- Content Wrapper -->
        <div id="content-wrapper" class="d-flex flex-column">

            <!-- Main Content -->
            <div id="content">

            <?php
             
             include('topbar.php');
             
             ?>

                <!-- Begin Page Content -->
                <div class="container-fluid">

                    <!-- Page Heading -->
                    <div class="d-sm-flex align-items-center justify-content-between mb-4">
                        <h1 class="h3 mb-0 text-gray-800">Employees</h1>

                     
                    </div>

                    <!-- Content Row -->



                   
                   
                            <!-- Department List -->
                        <div class="row">
                               

                        <div class="col-lg-12 mb-4">
                        <div class="card shadow mb-4">
                        <div class="card-header py-3">'
                        
                            <h6 class="m-0 font-weight-bold text-primary">Add Employee</h6>
                            
                        </div>
                   
                        <div class="card-body">
                           
                            <form action="" method="POST">

                                <div class="form-group row">
                                    <div class="col-sm-6 mb-3 mb-sm-0">
                                    <label for="">Employee ID</label>
                                        <input  type="text" class="form-control form-control-user" name="empid" 
                                            placeholder="Employee ID" required/>
                                    </div>
                                    <div class="col-sm-6 mb-3 mb-sm-0">
                                    <label for="">Department</label>
                                       <select  name="department" class="form-control form-control-user" id="exampleFormControlSelect1">
                                           <?php
                               
                                               
                                               $sql = custom_query("SELECT * FROM `tbl_department`");
                                               foreach ($sql as $row1) {
                                                   
                                                  
                                                   
                                           ?>
                                               
                                                       <option value="<?php echo $row1['d_name']; ?>"><?php echo $row1['d_name']; ?></option>

                                           <?php 
                                             
                                        
                                               }
                                            ?>

                                       </select>
                               </div> 
                                </div>

                                <div class="form-group row">
                                    <div class="col-sm-4">
                                    <label for="">First Name</label>
                                        <input type="text" class="form-control form-control-user"  name="fname" 
                                            placeholder="First Name" required>
                                    </div>
                                    <div class="col-sm-4">
                                    <label for=""> Middle Name</label>
                                        <input type="text" class="form-control form-control-user"  name="mname" 
                                            placeholder="Middle Name" required/>
                                    </div>
                                    <div class="col-sm-4">
                                    <label for="">Last Name</label>
                                        <input type="text" class="form-control form-control-user"  name="lname" 
                                            placeholder="Last Name" required/>
                                    </div>
                                </div>


                                <div class="form-group row">
                                    <div class="col-sm-12 mb-3 mb-sm-0">
                                    <label for="">Email</label>
                                        <input type="email" class="form-control form-control-user" name="email" 
                                            placeholder="Email" required/>
                                    </div>
                                   
                                </div>


                                <div class="form-group row">
                                    <div class="col-sm-12 mb-3 mb-sm-0">
                                    <label for="">Address</label>
                                        <input type="text" class="form-control form-control-user" name="address" 
                                            placeholder="Address" required/>
                                    </div>
                                   
                                </div>


                                <div class="form-group row">
                                    <div class="col-sm-6 mb-3 mb-sm-0">
                                    <label for="">Contact Number</label>
                                        <input type="number" class="form-control form-control-user"  name="contact" 
                                            placeholder="Contact Number" required/>
                                    </div>
                                    
                                    <div class="col-sm-6 mb-3 mb-sm-0">
                                    <label for="">Birthday</label>
                                        <input type="date" class="form-control form-control-user" name="bday" 
                                            placeholder="Birthday" required/>
                                    </div>
                                   
                                </div>
                                <div class="form-group row">
                                    <div class="col-sm-6 mb-3 mb-sm-0">
                                    <label for="">Are you experiencing? For the past month</label>
                                        <div class="form-check">
                                            <input class="form-check-input" name="fever" type="checkbox" value="yes" id="flexCheckDefault1">
                                            <label class="form-check-label" for="flexCheckDefault1">
                                                Fever
                                            </label>
                                        </div>
                                        <div class="form-check">
                                            <input class="form-check-input" name="headache" type="checkbox" value="yes" id="flexCheckDefault2">
                                            <label class="form-check-label" for="flexCheckDefault2">
                                                Headache
                                            </label>
                                        </div>
                                        <div class="form-check">
                                            <input class="form-check-input" name="sthroat" type="checkbox" value="yes" id="flexCheckDefault3">
                                            <label class="form-check-label" for="flexCheckDefault3">
                                                Sore Throat
                                            </label>
                                        </div>
                                        <div class="form-check">
                                            <input class="form-check-input" name="tassmell" type="checkbox" value="yes" id="flexCheckDefault4">
                                            <label class="form-check-label" for="flexCheckDefault4">
                                                Lost of Taste or Smell
                                            </label>
                                        </div>

                                    </div>
                                </div>

                                <div class="form-group row">
                                 
                                    
                                    <div class="col-sm-12 mb-3 mb-sm-0">
                                        <center><p>Note: QR will generate after registering employee.</p></center>
                                    </div>
                                   
                                </div>


                                <br>


                                <a class="btn btn-secondary" href="employee_list.php" type="button">Cancel</a>
                                <button class="btn btn-primary" type="submit">Save</button>



                            </form>
                        </div>
                    </div>
                        </div>
                    </div>

                </div>
                <!-- /.container-fluid -->

            </div>
            <!-- End of Main Content -->

           
<?php 
    include('footer.php');
?>