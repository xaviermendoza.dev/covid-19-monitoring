
<?php
include('header.php');
?>
    <!-- Page Wrapper -->
    <div id="wrapper">

    <?php
    
    include('nav.php');
    
    ?>

        <!-- Content Wrapper -->
        <div id="content-wrapper" class="d-flex flex-column">

            <!-- Main Content -->
            <div id="content">

             <?php
             
             include('topbar.php');
             
             ?>

                <!-- Begin Page Content -->
                <div class="container-fluid">

                    <!-- Page Heading -->
                    <div class="d-sm-flex align-items-center justify-content-between mb-4">
                        <h1 class="h3 mb-0 text-gray-800">Dashboard</h1>
                     
                    </div>

                    <!-- Content Row -->
                    <div class="row">

                        <!-- Number of Employee registered on log Card Example -->
                        <div class="col-xl-4 col-md-6 mb-4">
                            <div class="card border-left-primary shadow h-100 py-2">
                                <div class="card-body">
                                    <div class="row no-gutters align-items-center">
                                        <div class="col mr-2">
                                            <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">
                                                Daily Number of Employee registered on logs</div>

                                                <?php
                                                $de =date("Y/m/d");
                                                     $conn = getConnection();
                                                     $sql = "SELECT * FROM `tbl_logs` WHERE  `timein` <> '' AND `date`='$de' AND `department` <> 'Visitor'";
                                                     $result = $conn->query($sql);
                                                     $rowcount=count($result->fetchAll());



                                                ?>
                                            <div class="h5 mb-0 font-weight-bold text-gray-800"><?php echo $rowcount; ?></div>
                                        </div>
                                        <div class="col-auto">
                                            <i class="fas fa-pencil-alt fa-2x text-gray-300"></i>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!-- Daily Number of Visitors Card Example -->
                        <div class="col-xl-4 col-md-6 mb-4">
                            <div class="card border-left-success shadow h-100 py-2">
                                <div class="card-body">
                                    <div class="row no-gutters align-items-center">
                                        <div class="col mr-2">
                                        <?php
                                                     $de =date("Y/m/d");
                                                     $conn = getConnection();
                                                     $sql = "SELECT * FROM `tbl_logs` WHERE  `timein` <> '' AND `date`= '$de' ";
                                                     $result = $conn->query($sql);
                                                     $rowcount=count($result->fetchAll());



                                                ?>
                                            <div class="text-xs font-weight-bold text-success text-uppercase mb-1">
                                                Daily Number of Visitors</div>
                                            <div class="h5 mb-0 font-weight-bold text-gray-800"><?php echo $rowcount; ?></div>
                                        </div>
                                        <div class="col-auto">
                                            <i class="fas fa-dollar-sign fa-2x text-gray-300"></i>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!-- Number of Departments Card Example -->
                        <div class="col-xl-4 col-md-6 mb-4">
                            <div class="card border-left-info shadow h-100 py-2">
                                <div class="card-body">
                                    <div class="row no-gutters align-items-center">
                                        <div class="col mr-2">
                                        <?php
                                                     $de =date("Y/m/d");
                                                     $conn = getConnection();
                                                     $sql = "SELECT * FROM `tbl_department`";
                                                     $result = $conn->query($sql);
                                                     $rowcount=count($result->fetchAll());



                                                ?>
                                            <div class="text-xs font-weight-bold text-info text-uppercase mb-1">Number of Departments
                                            </div>
                                            <div class="row no-gutters align-items-center">
                                                <div class="col-auto">
                                                    <div class="h5 mb-0 mr-3 font-weight-bold text-gray-800"><?php echo $rowcount; ?></div>
                                                </div>
                                            
                                            </div>
                                        </div>
                                        <div class="col-auto">
                                            <i class="fas fa-building fa-2x text-gray-300"></i>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                   
                    </div>


                   
                   
                            <!-- Approach -->
                        <div class="row">
                               

                        <div class="col-lg-12 mb-4">

                          

                            
                            <div class="card shadow mb-4">
                                <div class="card-header py-3">
                                    <h6 class="m-0 font-weight-bold text-primary">Development Approach</h6>
                                </div>
                                <div class="card-body">
                                    <p>SB Admin 2 makes extensive use of Bootstrap 4 utility classes in order to reduce
                                        CSS bloat and poor page performance. Custom CSS classes are used to create
                                        custom components and custom utility classes.</p>
                                    <p class="mb-0">Before working with this theme, you should become familiar with the
                                        Bootstrap framework, especially the utility classes.</p>
                                </div>
                            </div>

                        </div>
                    </div>

                </div>
                <!-- /.container-fluid -->

            </div>
            <!-- End of Main Content -->

            
<?php 
    include('footer.php');
?>