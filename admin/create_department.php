<?php
include('header.php');

?>

<?php 
if($_SERVER['REQUEST_METHOD']== "POST"){

       
        $dname = $_POST['dname'];
        $dloc = $_POST['dloc'];
        $dpass = $_POST['dpass'];
        $drepass = $_POST['drepass'];


        $str= "select * from tbl_department where d_name=:u";
        $cm=$conn->prepare($str);
        $cm->bindParam(':u', $_POST['dname']);
      
        $cm->execute();
        $count = $cm->rowcount();
        
        if ($count == 0) {

            if($dpass == $drepass){
                $array = array(
                    'd_name'=>$dname,
                    'd_location' => $dloc,
                    'd_password' => md5($drepass)
                );
                if(insert($array,'tbl_department')){
                    ?>
                    <script>alert('Department Sucessfully Added');
                     window.location.href = './Department_list.php';
                    </script>
                    <?php 
                    
                }else{
                    ?>
                    <script>alert('Department not Added');</script>
                    <?php 
                }
    
            }else{
                ?>
                <script>alert('Password Did not Match');
                 window.location.href = './create_department.php';
                </script>
                <?php
            }

        }else{
            ?>
                <script>alert('Department Name Already Exist');
                window.location.href = './create_department.php';
                </script>
            <?php 
        }
      


       



}

?>


<?php 


?>
    <!-- Page Wrapper -->
    <div id="wrapper">

    <?php
    
    include('nav.php');
    
    ?>

        <!-- Content Wrapper -->
        <div id="content-wrapper" class="d-flex flex-column">

            <!-- Main Content -->
            <div id="content">

            <?php
             
             include('topbar.php');
             
             ?>

                <!-- Begin Page Content -->
                <div class="container-fluid">

                    <!-- Page Heading -->
                    <div class="d-sm-flex align-items-center justify-content-between mb-4">
                        <h1 class="h3 mb-0 text-gray-800">Department</h1>

                     
                    </div>

                    <!-- Content Row -->



                   
                   
                            <!-- Department List -->
                        <div class="row">
                               

                        <div class="col-lg-12 mb-4">
                        <div class="card shadow mb-4">
                        <div class="card-header py-3">'
                        
                            <h6 class="m-0 font-weight-bold text-primary">Add Department</h6>
                            
                        </div>
                   
                        <div class="card-body">
                           
                            <form action="" method="POST">

                                <div class="form-group row">
                                    <div class="col-sm-12 mb-3 mb-sm-0">
                                    <label for="">Department Name</label>
                                        <input type="text" class="form-control form-control-user"  name="dname" id="exampleFirstName"
                                            placeholder="Department Name" required/>
                                    </div>
                                   
                                </div>

                                <div class="form-group row">
                                    <div class="col-sm-12">
                                    <label for="">Department Location</label>
                                        <input type="text" class="form-control form-control-user"  name="dloc" id="exampleLastName"
                                            placeholder="Department Location" required/>
                                    </div>
                                </div>


                                <div class="form-group row">
                                    <div class="col-sm-6 mb-3 mb-sm-0">
                                    <label for="">Password</label>
                                        <input type="password" class="form-control form-control-user" id="exampleInputPassword"  name="dpass" placeholder="Password" required/>
                                    </div>
                                    <div class="col-sm-6">
                                    <label for="">Confirm Password</label>
                                        <input type="password" class="form-control form-control-user" id="exampleRepeatPassword" name="drepass" placeholder="Repeat Password" required/>
                                    </div>
                                </div><br>


                                <a class="btn btn-secondary" href="department_list.php" type="button">Cancel</a>
                                <button class="btn btn-primary" type="submit">Save</button>

                            </form>
                        </div>
                    </div>
                        </div>
                    </div>

                </div>
                <!-- /.container-fluid -->

            </div>
            <!-- End of Main Content -->

           
<?php 
    include('footer.php');
?>