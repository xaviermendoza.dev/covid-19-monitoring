<?php
include('header.php');

?>

<?php 
if($_SERVER['REQUEST_METHOD']== "POST"){







        $id = $_GET['id'];
        $email = $_POST['email'];
        $contact = $_POST['contact'];
        $address=$_POST['address'];
        $dept = $_POST['department'];


        if ($_POST['fever']==""){
            $fever= "No";
        }else{
            $fever = $_POST['fever'];
        }

        if ($_POST['headache']==""){
            $headache= "No";
        }else{
            $headache = $_POST['headache'];
        }

        if ($_POST['sthroat']==""){
            $sthroat= "No";
        }else{
            $sthroat = $_POST['sthroat'];
        }

        if ($_POST['tassmell']==""){
            $tassmell= "No";
        }else{
            $tassmell = $_POST['tassmell'];
        }
        




        

        
        
        $qrcode = $id .",". $_POST['fname'].",".$_POST['mname'].",".$_POST['lname'];

           
            require_once('../phpqrcode/qrlib.php');

            $path='../img/qrcode/';
            $file= $path.$_POST['empid'].".png";
    
    
            $text= $qrcode;
    
            QRcode::png($text, $file, 'L', 10);
            

        
        $array = array(
            'email' =>$email,
            'contact' => $contact,
            'address' => $address,
            'img_path' => $file,
            'department' =>$dept,
            'fever'=> $fever,
            'headache'=> $headache,
            'sthroat'=> $sthroat,
            'tassmell'=> $tassmell
        );

        if(update($array,$id,'tbl_employee')){
            ?>
            <script>alert('Employee Sucessfully Updated');
             window.location.href = './employee_list.php';
            </script>
            <?php 
            
        }else{
            ?>
            <script>alert('Employee not updated');</script>
            <?php 
        }
           



       



}

?>


<?php 


?>
    <!-- Page Wrapper -->
    <div id="wrapper">

    <?php
    
    include('nav.php');
    
    ?>

        <!-- Content Wrapper -->
        <div id="content-wrapper" class="d-flex flex-column">

            <!-- Main Content -->
            <div id="content">

            <?php
             
             include('topbar.php');
             
             ?>

                <!-- Begin Page Content -->
                <div class="container-fluid">

                    <!-- Page Heading -->
                    <div class="d-sm-flex align-items-center justify-content-between mb-4">
                        <h1 class="h3 mb-0 text-gray-800">Employees</h1>

                     
                    </div>

                    <!-- Content Row -->



                   
                   
                            <!-- Department List -->
                        <div class="row">
                               

                        <div class="col-lg-12 mb-4">
                        <div class="card shadow mb-4">
                        <div class="card-header py-3">'
                        
                            <h6 class="m-0 font-weight-bold text-primary">Update Employee</h6>
                            
                        </div>
                   
                        <div class="card-body">
                           
                            <form action="" method="POST">
                                <?php
                                
                                    $id=$_GET['id'];
                                    $data = custom_query("SELECT * FROM `tbl_employee` WHERE id=$id");
                                    foreach ($data as $row) {
                                        $depart = $row['department'];
                                   
                                ?>
                                <div class="form-group row">
                                    <div class="col-sm-6 mb-3 mb-sm-0">
                                    <label for="">Employee ID</label>
                                        <input  type="text" class="form-control form-control-user" value="<?php echo $row['employee_id']; ?>" name="empid" 
                                            placeholder="Employee ID" readonly/>
                                    </div> 
                                    <div class="col-sm-6 mb-3 mb-sm-0">
                                            <label for="">Department</label>
                                            <select  name="department" class="form-control form-control-user" id="exampleFormControlSelect1">
                                                <?php
                                    
                                                    
                                                    $sql = custom_query("SELECT * FROM `tbl_department`");
                                                    foreach ($sql as $row1) {
                                                        
                                                        if($row1['d_name']== $depart){
                                                        ?>
                                                             <option selected value="<?php echo $row1['d_name']; ?>"><?php echo $row1['d_name']; ?></option>
                                                        <?php
                                                        }else{

                                                        
                                                ?>
                                                    
                                                            <option value="<?php echo $row1['d_name']; ?>"><?php echo $row1['d_name']; ?></option>

                                                <?php 
                                                        }
                                                
                                                    }
                                                 ?>

                                            </select>
                                    </div> 
                                   
                                </div>

                                <div class="form-group row">
                                    <div class="col-sm-4">
                                    <label for="">First Name</label>
                                        <input type="text" class="form-control form-control-user" value="<?php echo $row['fname']; ?>" name="fname" 
                                            placeholder="First Name" readonly>
                                    </div>
                                    <div class="col-sm-4">
                                    <label for="">Middle Name</label>
                                        <input type="text" class="form-control form-control-user" value="<?php echo $row['mname']; ?>" name="mname" 
                                            placeholder="Middle Name" readonly/>
                                    </div>
                                    <div class="col-sm-4">
                                    <label for="">Last Name</label>
                                        <input type="text" class="form-control form-control-user" value="<?php echo $row['lname']; ?>" name="lname" 
                                            placeholder="Last Name" readonly/>
                                    </div>
                                </div>


                                <div class="form-group row">
                                    <div class="col-sm-12 mb-3 mb-sm-0">
                                    <label for="">Email</label>
                                        <input type="email" class="form-control form-control-user" value="<?php echo $row['email']; ?>" name="email" 
                                            placeholder="Email" required/>
                                    </div>
                                   
                                </div>


                                <div class="form-group row">
                                    <div class="col-sm-12 mb-3 mb-sm-0">
                                    <label for="">Address</label>
                                        <input type="text" class="form-control form-control-user" value="<?php echo $row['address']; ?>" name="address" 
                                            placeholder="Address" required/>
                                    </div>
                                   
                                </div>


                                <div class="form-group row">
                                    <div class="col-sm-6 mb-3 mb-sm-0">
                                    <label for="">Contact Number</label>
                                        <input type="number" class="form-control form-control-user" value="<?php echo $row['contact']; ?>" name="contact" 
                                            placeholder="Contact Number" required/>
                                    </div>
                                    
                                    <div class="col-sm-6 mb-3 mb-sm-0">
                                    <label for="">Birthday</label>
                                        <input type="text" value="<?php echo $row['birthday']; ?>" disabled class="form-control form-control-user" name="bday" 
                                            placeholder="Birthday" required/>
                                    </div>
                                   
                                </div>
                                <div class="form-group row">
                                    <div class="col-sm-6 mb-3 mb-sm-0">
                                    <label for="">Are you experiencing? For the past month</label>
                                        <?php
                                        echo $row['fever'];
                                        
                                            if($row['fever'] == "yes"){
                                                ?>
                                                    <div class="form-check">
                                                        <input class="form-check-input" name="fever" type="checkbox" value="yes" checked id="flexCheckDefault1">
                                                        <label class="form-check-label" for="flexCheckDefault1">
                                                            Fever
                                                        </label>
                                                    </div>
                                                <?php
                                            }else{
                                                ?>
                                                    <div class="form-check">
                                                        <input class="form-check-input" name="fever" type="checkbox" value="yes" id="flexCheckDefault1">
                                                        <label class="form-check-label" for="flexCheckDefault1">
                                                            Fever
                                                        </label>
                                                    </div>
                                                <?php
                                            }
                                        
                                        ?>
                                         <?php
                                        
                                        if($row['headache']  == "yes"){
                                            ?>
                                                <div class="form-check">
                                                    <input class="form-check-input" name="headache" type="checkbox" value="yes" checked id="flexCheckDefault2">
                                                    <label class="form-check-label" for="flexCheckDefault2">
                                                        Headache
                                                    </label>
                                                </div>
                                            <?php
                                        }else{
                                            ?>
                                                <div class="form-check">
                                                    <input class="form-check-input" name="headache" type="checkbox" value="yes" id="flexCheckDefault2">
                                                    <label class="form-check-label" for="flexCheckDefault2">
                                                        Headache
                                                    </label>
                                                </div>
                                            <?php
                                        }
                                    
                                    ?>
                                     <?php
                                        
                                        if($row['sthroat']  == "yes"){
                                            ?>
                                                <div class="form-check">
                                                    <input class="form-check-input" name="sthroat" type="checkbox" value="yes" checked id="flexCheckDefault3">
                                                    <label class="form-check-label" for="flexCheckDefault3">
                                                        Sore Throat
                                                    </label>
                                                </div>
                                            <?php
                                        }else{
                                            ?>
                                                <div class="form-check">
                                                    <input class="form-check-input" name="sthroat" type="checkbox" value="yes" id="flexCheckDefault3">
                                                    <label class="form-check-label" for="flexCheckDefault3">
                                                        Sore Throat
                                                    </label>
                                                </div>
                                            <?php
                                        }
                                    
                                    ?>
                                     <?php
                                        
                                        if($row['tassmell']  == "yes"){
                                            ?>
                                                <div class="form-check">
                                                    <input class="form-check-input" name="tassmell" type="checkbox" value="yes" checked id="flexCheckDefault4">
                                                    <label class="form-check-label" for="flexCheckDefault4">
                                                        Lost of Taste or Smell
                                                    </label>
                                                </div>
                                            <?php
                                        }else{
                                            ?>
                                                <div class="form-check">
                                                    <input class="form-check-input" name="tassmell" type="checkbox" value="yes" id="flexCheckDefault4">
                                                    <label class="form-check-label" for="flexCheckDefault4">
                                                        Lost of Taste or Smell
                                                    </label>
                                                </div>
                                            <?php
                                        }
                                    
                                    ?>

                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-sm-12 mb-3 mb-sm-0">
                                        <center>
                                            <img  src="<?php echo $row['img_path']; ?>" alt="">
                                            <p>Note: The QRCODE will change after saving.</p>
                                        </center>
                                        
                                    </div>
                                   
                                </div>

                                <br>


                                <a class="btn btn-secondary" href="employee_list.php" type="button">Cancel</a>
                                <button class="btn btn-primary" type="submit">Save</button>


                                <?php  } ?>
                            </form>
                        </div>
                    </div>
                        </div>
                    </div>

                </div>
                <!-- /.container-fluid -->

            </div>
            <!-- End of Main Content -->

           
<?php 
    include('footer.php');
?>