<?php
include('header.php');

?>

<?php 
if($_SERVER['REQUEST_METHOD']== "POST"){
    if(isset($_GET['id'])){
        $id = $_GET['id'];
        if(delete($id,'tbl_employee')){
            //deleted
            ?>
            <script>alert('Employee Deleted');
            window.location.href = 'employee_list.php';
            </script>
            <?php 
            
        }else{
            //not deleted
            ?>
            <script>alert('Department NOT Deleted');
            window.location.href = 'employee_list.php';
            </script>
            <?php 
       
        }

    }else{

        die();
    }
}

?>


<?php 


?>
    <!-- Page Wrapper -->
    <div id="wrapper">

    <?php
    
    include('nav.php');
    
    ?>

        <!-- Content Wrapper -->
        <div id="content-wrapper" class="d-flex flex-column">

            <!-- Main Content -->
            <div id="content">

            <?php
             
             include('topbar.php');
             
             ?>

                <!-- Begin Page Content -->
                <div class="container-fluid">

                    <!-- Page Heading -->
                    <div class="d-sm-flex align-items-center justify-content-between mb-4">
                        <h1 class="h3 mb-0 text-gray-800">Employees</h1>

                     
                    </div>

                    <!-- Content Row -->



                   
                   
                            <!-- Department List -->
                        <div class="row">
                               

                        <div class="col-lg-12 mb-4">
                        <div class="card shadow mb-4">
                        <div class="card-header py-3">'
                        
                            <h6 class="m-0 font-weight-bold text-primary">Delete Employee</h6>
                            
                        </div>
                   
                        <div class="card-body">
                            <h3>Are you sure deleting this Emoloyee - "<?php echo $_GET['name']; ?>"</h3>
                            <p class="mb-0">Select "Delete" below if you are ready to delete the Employee.</p><br>
                            <form action="" method="POST">
                                <a class="btn btn-secondary" href="employee_list.php" type="button">Cancel</a>
                                <button class="btn btn-danger" type="submit">Delete</button>
                            </form>
                        </div>
                    </div>
                        </div>
                    </div>

                </div>
                <!-- /.container-fluid -->

            </div>
            <!-- End of Main Content -->

           
<?php 
    include('footer.php');
?>