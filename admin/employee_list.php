<?php
include('header.php');

?>

<?php 


?>
    <!-- Page Wrapper -->
    <div id="wrapper">

    <?php
    
    include('nav.php');
    
    ?>

        <!-- Content Wrapper -->
        <div id="content-wrapper" class="d-flex flex-column">

            <!-- Main Content -->
            <div id="content">

            <?php
             
             include('topbar.php');
             
             ?>

                <!-- Begin Page Content -->
                <div class="container-fluid">

                    <!-- Page Heading -->
                    <div class="d-sm-flex align-items-center justify-content-between mb-4">
                        <h1 class="h3 mb-0 text-gray-800">Employees</h1>
                        <a href="create_employee.php" class="btn btn-primary">Add Employee</a>
                     
                    </div>

                    <!-- Content Row -->



                   
                   
                            <!-- Department List -->
                        <div class="row">
                               

                        <div class="col-lg-12 mb-4">
                        <div class="card shadow mb-4">
                        <div class="card-header py-3">'
                        
                            <h6 class="m-0 font-weight-bold text-primary">DataTables</h6>
                            
                        </div>
                   
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                                    <thead>

                                        <tr>
                                            <th>Employee ID</th>
                                            <th>First Name</th>
                                            <th>Last Name</th>
                                            <th></th>

                                        </tr>
                                    </thead>
                        

                        
                                    <tbody>

                                    <?php 

                                        $data = custom_query("SELECT * FROM `tbl_employee` WHERE `department`<>'Visitor'");
                                        foreach ($data as $row) {
                                            
                                           
                                            ?>

                                        <tr>
                                            
                                            <td><?php echo $row['employee_id']; ?></td>
                                            <td><?php echo $row['fname']; ?></td>
                                            <td><?php echo $row['lname']; ?></td>
                                            <td><a href="update_employee.php?id=<?php echo $row['ID'];  ?>" class="btn btn-primary"><i class="fas fa-pencil-alt"></i></a> <a href="delete_employee.php?id=<?php echo $row['ID'];  ?>&name=<?php echo $row['fname']; ?>" class="btn btn-danger"><i class="fas fa-trash"></i></a></td>

                                        </tr>

                                        <?php } ?>

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                        </div>
                    </div>

                </div>
                <!-- /.container-fluid -->

            </div>
            <!-- End of Main Content -->

           
<?php 
    include('footer.php');
?>