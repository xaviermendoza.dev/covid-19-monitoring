
<?php 
try {
    $constring = 'mysql:host=localhost;dbname=db_covidmonitor'; // host and db
    $user= 'root'; // define the username
    $pass=''; // password
    $conn = new PDO($constring, $user, $pass);
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        return $conn;
    } catch (PDOException $e) {
        $msg = $e -> getmessage();
        echo "ERROR LIST ->".$msg;
        die();
    }
    

?>